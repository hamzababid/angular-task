import { Component, OnInit } from '@angular/core';
import { Employee } from '../shared/employee';
import { Employees } from '../shared/mock-employee';
import { EmployeesService } from './employees.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  employees: Employee[];
  selectedEmployee: Employee;
  constructor(private employeesService: EmployeesService) { }

  ngOnInit() {
    this.getEmployees();
  }

  getEmployees(): void {
    this.employeesService.getEmployees()
    .subscribe(employees => this.employees = employees);
  }

}
