import { Injectable } from '@angular/core';
import { Employee } from '../shared/employee';
import { Employees } from '../shared/mock-employee';
import { Observable, of } from 'rxjs';
import { MessagesService } from '../messages/messages.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {

  constructor(private messagesService: MessagesService) { }

  getEmployees(): Observable<Employee[]> {
    this.messagesService.add('EmployeesService: fetched employees');
    return of(Employees);
  }

  getEmployee(id: number): Observable<Employee> {
    this.messagesService.add(`EmployeesService: fetched employee id=${id}`);
    return of(Employees.find(employee => employee.id === id));
  }
}
