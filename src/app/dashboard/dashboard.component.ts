import { Component, OnInit } from '@angular/core';
import { EmployeesService }  from '../employees/employees.service';
import { Employee } from '../shared/employee';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  employees: Employee[] = [];
  constructor(private employeesService: EmployeesService) { }

  ngOnInit() {
    this.getEmployees();
  }

  getEmployees(): void {
    this.employeesService.getEmployees()
      .subscribe(employees => this.employees = employees.slice(1, 5));
  }

}
