import { Employee } from './employee';

export const Employees: Employee[] = [
  { id: 11, name: 'Hamza' },
  { id: 12, name: 'Taha' },
  { id: 13, name: 'Ammar' },
  { id: 14, name: 'Saqib' },
  { id: 15, name: 'Talha' },
  { id: 16, name: 'Imran' },
  { id: 17, name: 'Arsalan' },
  { id: 18, name: 'Tauseen' },
  { id: 19, name: 'Sher' },
  { id: 20, name: 'Shaheen' }
];